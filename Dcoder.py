#Decoder

alpha="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
str_in=input("Enter message,likeHELLO:")
shift=input("How much was the applied shift? ")
str_out=""
try:
    shift = -int(shift)
except:
    print("Invallid shift input")
    raise ValueError
str_out=""
for i in str_in:
    if i not in alpha:
        print("Invalid str input")
        raise ValueError
    res_index = alpha.index(i)+(shift%26)
    if res_index > 25:
        res_index -= 26
    elif res_index < 0:
        res_index += 26
    str_out += alpha[res_index]
print(str_out)